package com.tw.cs;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CircleTest {
    
    @Test
    public void shouldRepresentCircleInString() {
        Circle circle = new Circle(3);
        
        String expected =  "  ***  \n" +
                           " *   * \n" +
                           "*     *\n" +
                           "*     *\n" +
                           "*     *\n" +
                           " *   * \n" +
                           "  ***  " ;
                           
        assertEquals(expected, circle.toString());        

    }
    
}