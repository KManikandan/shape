package com.tw.cs;

import java.util.List;
import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Collectors;

public class Display {
    
    public String draw(List<Shape> squares) {
        String result = "";
        for(Shape square: squares) {
            result = mergeShape(result, square.toString());            
        }
        return result;
    }
    
    public String mergeShape(String previous, String current) {
        List<String> previousShape = Arrays.asList(previous.split("\n"));
        List<String> currentShape = Arrays.asList(current.split("\n"));
        int min = Math.min(previousShape.size(), currentShape.size());
        int max = Math.max(previousShape.size(), currentShape.size());

        String mergeShape = IntStream.range(0, min)
                        .mapToObj(i -> mergeRow(previousShape.get(i), currentShape.get(i)))
                        .collect(Collectors.joining("\n"));
                        
        List<String> tailList = previousShape.size() > currentShape.size() ? previousShape.subList(min, max) : currentShape.subList(min, max);                        
        return String.join("\n", mergeShape, tailList.stream().collect(Collectors.joining("\n")));
        
    }
    
    public String mergeRow(String previous, String current) {
        int min = Math.min(previous.length(), current.length());
        int max = Math.max(previous.length(), current.length());
        
        String headRow = IntStream.range(0, min)
                        .mapToObj(i -> mergePoint(i, previous, current))
                        .collect(Collectors.joining());
                        
        String tailRow = previous.length() > current.length() ? previous.substring(min, max) : current.substring(min, max);                        
        return String.join("",headRow, tailRow);
    }
    
    private String mergePoint(int index, String row1, String row2) {
        Character result = row2.charAt(index) == ' ' ? row1.charAt(index) : row2.charAt(index);
        return result.toString();
    }

    
    
}