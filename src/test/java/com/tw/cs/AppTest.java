package com.tw.cs;

import java.util.Arrays;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class AppTest {
    
    @Test
    public void shouldTest() {
        assertEquals(4, new App().count(Arrays.asList(1,2,3,4)));
    }
}
