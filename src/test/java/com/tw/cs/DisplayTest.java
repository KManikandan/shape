package com.tw.cs;

import org.junit.Test;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

public class DisplayTest {
    
    @Test
    public void shouldDrawSingleSquare() {
        Display display = new Display();
        
        String shape = display.draw(asList(new Square(2)));
        
        assertEquals("xx\nxx", shape);
    }
    
    @Test
    public void shouldDrawOneSquareOnTopOfAnotherSquare() {
        Display display = new Display();
        
        String shape = display.draw(asList(new Square(3), new Square(5)));
        
        String expected = "xxxxx\n"+
                          "x x x\n"+
                          "xxx x\n"+
                          "x   x\n"+
                          "xxxxx";
        
        assertEquals(expected, shape);
    }
    
        @Test
    public void shouldDrawCircleOnTopOfSquare() {
        Display display = new Display();
        
        String shape = display.draw(asList(new Square(10), new Circle(3)));
        
        String expected =  "xx***xxxxx\n" +
                           "x*   *   x\n" +
                           "*     *  x\n" +
                           "*     *  x\n" +
                           "*     *  x\n" +
                           "x*   *   x\n" +
                           "x ***    x\n" +        
                           "x        x\n" +        
                           "x        x\n" +        
                           "xxxxxxxxxx" ;        
        
        assertEquals(expected, shape);
    }


    
    
}