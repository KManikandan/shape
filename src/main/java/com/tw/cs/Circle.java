package com.tw.cs;

import java.util.stream.IntStream;

import java.util.stream.Collectors;
import static java.util.stream.Collectors.toList;


public class Circle implements Shape{
    
    private int radius;
    public static final String EDGE = "*";
    public static final String NON_EDGE = " ";

    
    public Circle(int radius) {
        this.radius = radius;
    }
    
    @Override
    public String toString() {
        return IntStream.rangeClosed(0, radius * 2)
                .mapToObj(this::row)
                .collect(Collectors.joining("\n"));
    }
    
    public String row(int rowNumber) {
        return IntStream.rangeClosed(0,radius * 2)
                        .mapToObj(columnNumber -> isEdge(rowNumber, columnNumber) ? EDGE : NON_EDGE )
                        .collect(Collectors.joining());
    }
    
    private boolean isEdge(int row, int column) {
        int x = Math.abs(row - radius);
        int y = Math.abs(column - radius);
        double distanceFromCenter = Math.sqrt(x*x + y*y);
        
        return (int) Math.round(distanceFromCenter) == radius;
    }

}