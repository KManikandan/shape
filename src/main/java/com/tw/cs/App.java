package com.tw.cs;

import java.util.List;

public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }
    
    public long count(List<Integer> numbers) {
        return numbers.stream().count();
    }
}
