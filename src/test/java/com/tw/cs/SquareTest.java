package com.tw.cs;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SquareTest {
    
    @Test
    public void shouldRepresentUnitSquare() {
        Square square = new Square(1);
        
        assertEquals("x", square.toString());
    }
    
    @Test
    public void shouldRepresentSquareOfSize2() {
        Square square = new Square(2);
        

        assertEquals("xx\nxx", square.toString());
    }
    
    @Test
    public void shouldRepresentSquareOfSize4() {
        Square square = new Square(4);
        
        assertEquals("xxxx\nx  x\nx  x\nxxxx", square.toString());
    }

    
    

}