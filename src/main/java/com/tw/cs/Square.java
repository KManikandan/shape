package com.tw.cs;

import java.util.stream.IntStream;

import java.util.stream.Collectors;
import static java.util.stream.Collectors.toList;

public class Square implements Shape{
    private int size;
    public static final String EDGE = "x";
    public static final String NON_EDGE = " ";
    public static final int STARTING_INDEX = 1;

    
    Square(int size) {
        this.size = size;
    }
    
    @Override
    public String toString() {
        return IntStream.rangeClosed(STARTING_INDEX, size)
                .mapToObj(this::row)
                .collect(Collectors.joining("\n"));
    }
    
    public String row(int rowNumber) {
        return IntStream.rangeClosed(STARTING_INDEX,size)
                        .mapToObj(columnNumber -> isEdge(rowNumber, columnNumber) ? EDGE : NON_EDGE )
                        .collect(Collectors.joining());
    }
    
    private boolean isEdge(int row, int column) {
        return row == STARTING_INDEX || row == size || column == STARTING_INDEX || column == size;
    }
}